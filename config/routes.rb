Rails.application.routes.draw do
  # get 'books/index'

  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  namespace :api do
    namespace :v1 do
      resources :authors, only: [:index, :create, :update, :destroy] do
        resources :books, only: [:create, :update]
      end

      resources :books, only: [:index, :destroy] do
        collection do
          get '/:type/:id/' , to: "books#index"
        end
      end

      resources :users, only: [:index, :create, :update, :destroy] do
        resources :books, only: [] do
          collection do
            post :issue
            post :return
          end 
        end
      end

      

    end
  end
end
