class AddIssuedToBooks < ActiveRecord::Migration[7.0]
  def change
    add_column :books, :issued, :boolean, :default => false
  end
end
