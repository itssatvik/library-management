class MakeColumnNotNull < ActiveRecord::Migration[7.0]
  def change
    change_column_null :authors, :name, false
    change_column_null :books, :name, false
    change_column_null :users, :name, false
  end
end
