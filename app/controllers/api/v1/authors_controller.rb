module Api
  module V1
    class AuthorsController < ApplicationController
      skip_before_action :verify_authenticity_token
    
      def index
        render json: {authors: Author.all}
      end
    
      def create
        author = Author.create(create_allowed_params)
        if author.valid?
          return render json: {status: 'success', message: "author created successfully", author: author}
        end
        render json: {status: 'failure', message: author.errors.full_messages.to_sentence}
      end
    
      def destroy
        return render json: {status: 'failure', message: "author not found"}, status: :not_found  unless author
        
        if author.destroy
          return render json: {status: 'success', message: "author deleted successfully"}
        end
        render json: {status: 'failure', message: author.errors.full_messages.to_sentence}

      end
    
      private
      def create_allowed_params
        params.require(:author).permit(:name)
      end

      def author
        author ||= Author.find_by(id: params[:id])
      end

    end
  end
end


