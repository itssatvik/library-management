module Api
  module V1
    class UsersController < ApplicationController
      skip_before_action :verify_authenticity_token
    
      def index
        render json: {users: User.all}
      end
    
      def create
        user = User.create(create_allowed_params)
        if user.valid?
          return render json: {status: 'success', message: "user created successfully", user: user}
        end
        render json: {status: 'failure', message: user.errors.full_messages.to_sentence}
      end
      #dummy master commit
      def destroy
        return render json: {status: 'failure', message: "user not found"}, status: :not_found  unless user
        if user.destroy
          return render json: {status: 'success', message: "user deleted successfully"}
        end
        render json: {status: 'failure', message: user.errors.full_messages.to_sentence}
      end
    
      private
      def create_allowed_params
        params.require(:user).permit(:name)
      end

      def user
        user ||= User.find_by(id: params[:id])
      end

    end
  end
end
