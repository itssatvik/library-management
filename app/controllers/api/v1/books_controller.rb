module Api
  module V1
    class BooksController < ApplicationController
      skip_before_action :verify_authenticity_token
#eee
      def index
        if params[:type]
          entity_type = params[:type] == 'author' ? author : user
          return render json: {status: 'failure', message: "#{params[:type]} not found"}, status: :not_found  unless entity_type

          entity_type == author ? (return render json: {status: 'success', books: author.books}) : (return render json: {status: 'success', books: user.books})
        end
        render json: {status: 'success', books: Book.where(issued: false)}
      end
    
      def create
        return render json: {status: 'failure', message: "author not found"}, status: :not_found  unless author

        book = author.books.create(create_allowed_params)
        if book.valid?
          return render json: {status: 'success', message: "book created successfully", book: book}
        end
        render json: {status: 'failure', message: book.errors.full_messages.to_sentence}
      end
      
      def destroy
        return render json: {status: 'failure', message: "book not found"}, status: :not_found  unless book

        if book.destroy
          return render json: {status: 'success', message: "book deleted successfully"}
        end
        render json: {status: 'failure', message: book.errors.full_messages.to_sentence}, status: :bad_request
      end
    
    
      def issue
        return render json: {status: 'failure', message: "user not found"}, status: :not_found  unless user

        book = find_book_to_process(issue_status = false)
        if book.nil?
          return render json: {status: 'failure', message: "book not found"}, status: :not_found
        end

        book.assign_attributes(user: user, issued: true)
        if book.save
          return render json: {status: 'success', message: "book issued successfully", book: book}
        end
        render json: {status: 'failure', message: book.errors.full_messages.to_sentence} , status: :bad_request
      end
    
      def return
        return render json: {status: 'failure', message: "user not found"}, status: :not_found  unless user

        book = find_book_to_process(issue_status = true) 
        if book.nil?
          return render json: {status: 'failure', message: "book not found"}, status: :not_found
        end

        book.assign_attributes(user: nil, issued: false)
        if book.save
          return render json: {status: 'success', message: "book returned successfully", book: book}
        end
        render json: {status: 'failure', message: book.errors.full_messages.to_sentence} , status: :bad_request
      end
    
      private
      def create_allowed_params
        params.require(:book).permit(:name)
      end
    
      def find_book_to_process(issue_status)
        Book.find_by(id: params[:book_id], issued: issue_status)
      end

      def book
        book ||= Book.find_by(id: params[:id])
      end

      def user
        user ||= (User.find_by(id: params[:user_id]) || User.find_by(id: params[:id]))
      end

      def author
        author ||= (Author.find_by(id: params[:author_id]) || Author.find_by(id: params[:id]))
      end

    end
  end
end
