class Book < ApplicationRecord
  belongs_to :author
  belongs_to :user, optional: true
  validates :name, presence: true
end
